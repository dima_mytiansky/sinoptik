<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_210613_locality_name_tbl extends Migration
{
    public function up()
    {
        $this->createTable('locality_name', [
            'id' => Schema::TYPE_PK,
            'name' => 'varchar(64)',
            'locality_id' => Schema::TYPE_INTEGER,
            'language_id' => Schema::TYPE_INTEGER,
        ]);

        $this->createIndex('ix_locality_id_locality_name', 'locality_name', 'locality_id');
        $this->createIndex('ix_language_id_locality_name', 'locality_name', 'language_id');
    }

    public function down()
    {
        $this->dropTable('locality_name');
    }
}
