<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_203402_source_tbl extends Migration
{
    public function up()
    {
        $this->createTable('source', [
            'id' => Schema::TYPE_PK,
            'name' => 'varchar(64)',
            'latitude' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0',
            'longitude' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0',
            'temperature' => 'tinyint',
        ]);

        $this->createIndex('ix_source name', 'source', 'name');
    }

    public function down()
    {
        $this->dropTable('source');
    }
}
