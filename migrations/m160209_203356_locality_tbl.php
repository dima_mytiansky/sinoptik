<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_203356_locality_tbl extends Migration
{
    public function up()
    {
        $this->createTable('locality', [
            'id' => Schema::TYPE_PK,
            'population' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'latitude' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0',
            'longitude' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0',
            'source_id' => Schema::TYPE_INTEGER,
        ]);

        $this->createIndex('ix_locality_population', 'locality', 'population');
    }

    public function down()
    {
        $this->dropTable('locality');
    }
}
