<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Locality */

$this->title = 'Обновить нас. пункт: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Нас. пункты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="locality-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
