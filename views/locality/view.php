<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Locality */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Нас. пункты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви уверены что хотите удалить елемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nameUa',
            'nameRu',
            'nameEn',
            'population',
            'latitude',
            'longitude',
            'source.name',
        ],
    ]) ?>

</div>
