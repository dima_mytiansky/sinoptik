<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "source".
 *
 * @property integer $id
 * @property string $name
 * @property double $latitude
 * @property double $longitude
 * @property integer $temperature
 */
class Source extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'temperature', 'name'], 'required'],
            [['latitude', 'longitude'], 'number'],
            [['temperature'], 'integer'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название источника',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'temperature' => 'Температура',
        ];
    }

    public static function findClosestSource($lat, $lng)
    {
        $distanceExpression = new Expression("
        6371 * 2 * ASIN(SQRT(
            POWER(SIN(($lat - abs(latitude)) * pi()/180 / 2),
            2) + COS($lat * pi()/180 ) * COS(abs(latitude) *
            pi()/180) * POWER(SIN(($lng - longitude) *
            pi()/180 / 2), 2) ))
        ");
        return Source::find()->select([
            Source::tableName() . '.*',
            "$distanceExpression AS distance_km"
        ])->orderBy(['distance_km' => SORT_ASC])->one();
    }
}
