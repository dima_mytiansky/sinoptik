<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "locality".
 *
 * @property integer $id
 * @property integer $population
 * @property double $latitude
 * @property double $longitude
 * @property integer $source_id
 */
class Locality extends \yii\db\ActiveRecord
{

    const MIN_SOURCE_DISTANCE_KM = 100;

    public $nameUa;
    public $nameRu;
    public $nameEn;

    const LANGUAGE_UA = 1;
    const LANGUAGE_RU = 2;
    const LANGUAGE_EN = 3;

    public static $languageAttributes = [
        self::LANGUAGE_UA => 'nameUa',
        self::LANGUAGE_RU => 'nameRu',
        self::LANGUAGE_EN => 'nameEn',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nameUa', 'nameRu', 'nameEn'], 'safe'],
            [['population', 'source_id'], 'integer'],
            [['latitude', 'longitude'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'population' => 'Население',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'source_id' => 'Источник',
            'nameUa' => 'Название укр.',
            'nameRu' => 'Название рус.',
            'nameEn' => 'Название англ.',
        ];
    }

    public function getNames()
    {
        return $this->hasMany(LocalityName::className(), ['locality_id' => 'id']);
    }

    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }

    public function getSourceOptions()
    {
        $distanceExpression = new Expression("
        6371 * 2 * ASIN(SQRT(
            POWER(SIN(($this->latitude - abs(latitude)) * pi()/180 / 2),
            2) + COS($this->latitude * pi()/180 ) * COS(abs(latitude) *
            pi()/180) * POWER(SIN(($this->longitude - longitude) *
            pi()/180 / 2), 2) ))
        ");

        $sourcesQuery = Source::find()->select([Source::tableName() . '.*']);

        if ($this->isNewRecord) {
            $optionClosest = ['Ближайший'];
        } else {
            $sourcesQuery->addSelect(["$distanceExpression AS distance_km"])
            ->where(['<' , $distanceExpression, self::MIN_SOURCE_DISTANCE_KM])
            ->orderBy(['distance_km' => SORT_ASC]);
            $optionClosest = [];
        }

        $options = ArrayHelper::map($sourcesQuery->all(), 'id', 'name');

        return ArrayHelper::merge($optionClosest, $options);
    }

    public function afterFind()
    {
        foreach ($this->names as $name) {
            $attributeName = self::$languageAttributes[$name->language_id];

            $this->{$attributeName} = $name->name;
        }
    }

    public function afterSave()
    {
        foreach (self::$languageAttributes as $languageId => $languageAttribute) {
            if (!$this->{$languageAttribute}) {
                continue;
            }

            $localityQuery = LocalityName::find()
                ->where(['language_id' => $languageId, 'locality_id' => $this->id]);

            if ($localityQuery->exists()) {
                $localityName = $localityQuery->one();
                $localityName->name = $this->{$languageAttribute};
                $localityName->save();
            } else {
                $localityName = new LocalityName();
                $localityName->name = $this->{$languageAttribute};
                $localityName->language_id = $languageId;
                $this->link('names', $localityName);
            }
        }

        if (!$this->source_id) {

        }
    }

    public function beforeDelete()
    {
        $this->unlinkAll('names', true);

        return true;
    }

    public function beforeValidate()
    {
        foreach (self::$languageAttributes as $languageId => $languageAttribute) {
            if ($this->{$languageAttribute}) {
                return true;
            }
        }

        $this->addError(reset(self::$languageAttributes), 'Укажите хотя бы одно имя');

        return false;
    }
}
