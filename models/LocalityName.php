<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locality_name".
 *
 * @property integer $id
 * @property string $name
 * @property integer $locality_id
 * @property integer $language_id
 */
class LocalityName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locality_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locality_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'locality_id' => 'Locality ID',
            'language_id' => 'Language ID',
        ];
    }
}
