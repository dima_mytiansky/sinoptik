<?php

return [
    'adminEmail' => 'admin@example.com',
    'languages' => ['ua', 'ru', 'en'],
];
