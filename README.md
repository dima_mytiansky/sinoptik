Sinoptic
============================

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.

INSTALLATION
------------

1. Завантажити та розархівувати репозиторій [за посиланням](https://bitbucket.org/dima_mytiansky/sinoptik/get/0f569621c6a6.zip) в папку sinoptik
2. Встановити залежності
```bash
cd sinoptik
composer install
```

CONFIGURATION
-------------

### Database

1. Створити 
```sql
create database sinoptik character set 'utf8';
```
і налаштувати доступ бд в `config/db.php`:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=sinoptik',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```
2. Запустити міграції `php yii migrate up`
3. Розгорнути дамп з даними `mysql -uroot sinoptik < sinoptik.sql`

Запустити вбудований веб-сервер
```bash
cd web
php -S localhost:8080
```